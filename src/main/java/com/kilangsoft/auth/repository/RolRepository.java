package com.kilangsoft.auth.repository;


import com.kilangsoft.auth.bo.Rol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Interfaz que actua como repositorio para la entidad RoleOAuth.
 * 
 * @author Brayan Hamer Rodriguez Sanchez
 *
 */
@Repository
public interface RolRepository extends CrudRepository<Rol, UUID> {
	Rol findByNombre(String nombre);
	Rol findByUuid(UUID uuid);
}
