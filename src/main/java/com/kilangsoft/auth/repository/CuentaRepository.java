package com.kilangsoft.auth.repository;

import com.kilangsoft.auth.bo.Cuenta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Interfaz que actua como repositorio para la entidad User.
 *
 * @author Brayan Hamer Rodriguez Sanchez
 */
@Repository
public interface CuentaRepository extends CrudRepository<Cuenta, UUID> {
    Cuenta findByUsername(String username);

    Cuenta findByUuid(UUID uuid);

    Cuenta findByUsuarioUuid(UUID uuid);

    Cuenta findByUsuarioCedula(String cedula);
}