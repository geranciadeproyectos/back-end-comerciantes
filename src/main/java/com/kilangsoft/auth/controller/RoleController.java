package com.kilangsoft.auth.controller;


import com.kilangsoft.auth.service.RoleService;
import com.kilangsoft.auth.bo.Rol;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = { "comerciantes/api/v1/auth2" }, produces = { "application/json" })
@Api(value = "/role", description = "Operaciones Relacionadas con los roles conifgurados", consumes = "application/json")
public class RoleController {

	private static final Logger log = Logger.getLogger(RoleController.class);

	private RoleService roleService;

	@Autowired
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}

	@ApiOperation(value = "Obtiene todos los roles registrados", notes = "Devuelve una lista de todas las autoridades de la aplicacion", httpMethod = "GET")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Lista de roles", response= Rol.class, responseContainer = "List")
	})
	@RequestMapping(value = "/role", method = { RequestMethod.GET }, produces = { "application/json" })
	public List<Rol> getAll() {
		return (List<Rol>) roleService.getAll();
	}
}
