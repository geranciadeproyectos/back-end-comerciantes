package com.kilangsoft.auth.controller;

import com.kilangsoft.auth.service.UserService;
import com.kilangsoft.auth.bo.Cuenta;
import io.swagger.annotations.*;
import javassist.tools.rmi.ObjectNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Controlador REST que permite realizar operaciones relacionadas a los usuarios
 * de la aplicacion.
 * 
 * @author Brayan Hamer Rodriguez Sanchez
 *
 */
@RestController
@Api(value = "/user", description = "Operaciones Relacionadas con las cuentas de usuario", consumes = "application/json")
@RequestMapping(value = { "comerciantes/api/v1/auth2" }, produces = { "application/json" })
public class UserController {
	private static final Logger log = Logger.getLogger(UserController.class);

	private UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}


	@ApiOperation(value = "Obtiene todos los usuarios registrados", notes = "Obtiene una lista de todos los usuarios registrados en la aplicacion", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Detalles del usuario", response = UserDetails.class) })
	@RequestMapping(value = "/user", method = { RequestMethod.GET }, produces = { "application/json" })
	public Iterable<Cuenta> getUsers() {
		return userService.getAll();
	}


	@ApiOperation(value = "Obtiene todos los datos del usuario", notes = "Obtiene los detalles del usuario que envia la peticion", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Detalles del usuario", response = UserDetails.class) })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/user/detail", method = { RequestMethod.GET }, produces = { "application/json" })
	public UserDetails getUserDetail(OAuth2Authentication auth) {
		log.info(auth.getUserAuthentication().getName());
		return userService.loadUserByUsername(auth.getUserAuthentication().getName());
	}


	@ApiOperation(value = "Obtiene los datos del usuario", notes = "Obtiene la informacion basica del usuario que envia la peticion", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Datos del usuario", response = Cuenta.class) })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/user/info" , method = { RequestMethod.GET }, produces = { "application/json" })
	public Cuenta getUser(OAuth2Authentication auth) {
		log.info(auth.getUserAuthentication().getName());
		return userService.getUserByUserName(auth.getUserAuthentication().getName());
	}


	@ApiOperation(value = "Obtiene los datos de un usuario", notes = "Obtiene la informacion basica de un usuario dado su id", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Datos del usuario", response = Cuenta.class) })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/user/{id}", method = { RequestMethod.GET }, produces = { "application/json" })
	public Cuenta getUser(@ApiParam(value = "id del usuario a buscar", required = true) @PathVariable UUID id) {
		return userService.getUserById(id);
	}


	@ApiOperation(value = "Crea un usuario", notes = "Crea un nuevo usuario en la aplicacion", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 201, message = "Usuario creado", response = Cuenta.class) })
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/user", method = { RequestMethod.POST }, produces = { "application/json" })
	public Cuenta create(@ApiParam(value = "usuario a crear", required = true) @RequestBody Cuenta user) {
		user.setUuid(null);
		log.info("Realizando proceso de registro de usuario");

		return userService.CreateAccount(user);
	}

	@ApiOperation(value = "Actualiza un usuario", notes = "Edita los datos de un usuario excepto la contrasena", httpMethod = "PUT")
	@ApiResponses({ @ApiResponse(code = 200, message = "Usuario editado", response = Cuenta.class) })	
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/user", method = { RequestMethod.PUT }, produces = { "application/json" })
	public Cuenta update(@ApiParam(value = "id del usuario a editar", required = true) @RequestParam UUID id,
			@ApiParam(value = "usuario que contiene los nuevos datos", required = true) @RequestBody Cuenta user) {
		user.setUuid(null);
		log.info("Realizando proceso de actualizacion de usuario");

		return userService.updateAccount(id, user);
	}


	@ApiOperation(value = "Edita la contrasena de un usuario", notes = "Cambia la contrasena de un usuario dado su id", httpMethod = "PUT")
	@ApiResponses({ @ApiResponse(code = 200, message = "Contrasena cambiada", response = Boolean.class) })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/userPassword", method = { RequestMethod.PUT }, produces = { "application/json" })
	public Boolean updatePassword(
			@ApiParam(value = "id del usuario a cambiar la contrasena", required = true) @RequestParam UUID id,
			@ApiParam(value = "nueva contrasena", required = true) @RequestParam String password) {
		log.info("Realizando proceso de actualizacion de password de usuario con id " + id);
		return userService.updatePassword(id, password);
	}


	@ApiOperation(value = "Cambia la contrasena del usuario", notes = "Edita la contrasena del usuario que envia la solicitud", httpMethod = "PUT")
	@ApiResponses({ @ApiResponse(code = 200, message = "Contrasena cambiada", response = void.class) })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/changePassword", method = { RequestMethod.PUT }, produces = { "application/json" })
	public void changePassword(OAuth2Authentication auth,
			@ApiParam(value = "contrasena actual", required = true) @RequestParam String password,
			@ApiParam(value = "nueva contrasena", required = true) @RequestParam String newPassword,
			@ApiParam(value = "confirmacion de la nueva contrasena", required = true) @RequestParam String confirmNewPassword)
			throws ObjectNotFoundException {
		userService.changePassword(auth.getUserAuthentication().getName(), password, newPassword, confirmNewPassword);
	}

	@ApiOperation(value = "Borra un usuario", notes = "Elimina un usuario dado su id", httpMethod = "DELETE")
	@ApiResponses({ @ApiResponse(code = 204, message = "Contrasena cambiada", response = void.class) })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "/user", method = { RequestMethod.DELETE }, produces = { "application/json" })
	public void delete(@ApiParam(value = "id del usuario a borrar", required = true) @RequestParam UUID id) {
		log.info("Realizando proceso de eliminacion de usuario");
		userService.delete(id);
	}
}