package com.kilangsoft.auth.bo;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class UserOAuth extends Cuenta implements UserDetails {
    private static final Logger log = Logger.getLogger(UserOAuth.class);
    public UserOAuth() {
    }

    public UserOAuth(Cuenta user) {
        super(user);
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<RoleOAuth> roles = new ArrayList<>();
        roles.add(new RoleOAuth(getUsuario().getRol()));
        return roles;
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
