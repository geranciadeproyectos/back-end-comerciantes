package com.kilangsoft.auth.bo;

import org.springframework.security.core.GrantedAuthority;


public class RoleOAuth extends Rol implements GrantedAuthority {

    public RoleOAuth() {
        super();
    }

    public RoleOAuth(Rol rol) {
        super(rol);
    }

    @Override
    public String getAuthority() {
        return "ROLE_"+getNombre();
    }

    public void setAuthority(String name) {
        setNombre(name);
    }

    @Override
    public String toString() {
        return "RoleOAuth{} " + getAuthority();
    }
}