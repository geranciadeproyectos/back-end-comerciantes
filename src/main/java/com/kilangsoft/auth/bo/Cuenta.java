package com.kilangsoft.auth.bo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kilangsoft.usuario.bo.Usuario;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.UUID;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Cuenta {
    @Id
    @Column(name = "id")
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    @org.hibernate.annotations.Type(type = "pg-uuid")
    private UUID uuid;



    @NotEmpty
    @Column(unique = true,nullable = false)
    private String username;

    @NotEmpty
    @Column(nullable = false)
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    private Usuario usuario;


    public Cuenta(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Cuenta(String username, String password, Usuario usuario) {
        this.username = username;
        this.password = password;
        this.usuario = usuario;
    }

    public Cuenta() {
    }

    public Cuenta(Cuenta cuenta){
        this.username = cuenta.getUsername();
        this.password = cuenta.getPassword();
        this.usuario = cuenta.getUsuario();
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "uuid=" + uuid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", usuario=" + usuario +
                '}';
    }
}
