package com.kilangsoft.auth.service;


import com.kilangsoft.auth.bo.UserOAuth;
import com.kilangsoft.auth.repository.CuentaRepository;
import com.kilangsoft.auth.bo.Cuenta;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.UUID;


/**
 * Implementacion del servicio que manejan los Usuarios.
 *
 * @author Brayan Hamer Rodriguez Sanchez
 */
@Service
public class UserService implements UserDetailsService {

    private static final Logger log = Logger.getLogger(UserService.class);

    private CuentaRepository cuentaRepository;
    private BCryptPasswordEncoder bcryptEncoder;
    private RoleService roleService;

    @Autowired
    public UserService(CuentaRepository cuentaRepository, BCryptPasswordEncoder bcryptEncoder, RoleService roleService) {
        this.cuentaRepository = cuentaRepository;
        this.bcryptEncoder = bcryptEncoder;
        this.roleService = roleService;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Cuenta user = cuentaRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("El usuario %s no existe", username));
        }
        return new UserOAuth(user);
    }

    public Cuenta getUserByUserName(String username) {
        return cuentaRepository.findByUsername(username);
    }

    public Cuenta getUserById(UUID id) {
        return cuentaRepository.findByUuid(id);
    }

    public boolean validateFreeAccount(String username) {
        if (null != cuentaRepository.findByUsername(username)) {
            throw new RuntimeException("El usuario "+username+"Ya se encuentra registrado.");
        }
        return true;
    }

    public Cuenta CreateAccount(Cuenta user) {
        log.info("validando que el usuario no se encuentre registrado");
        if (null != cuentaRepository.findByUsername(user.getUsername())) {
            throw new RuntimeException("El usuario "+user.getUsername()+"ya se encuentra registrado.");
        }
        log.debug("Encriptando contraseña del usuario");
        log.debug("Crudo [" + user.getPassword() + "]");
        user.setPassword(bcryptEncoder.encode(user.getPassword()));
        log.debug("Encriptada [" + user.getPassword() + "]");
        user = save(user);
        return user;
    }

    public Cuenta updateAccount(UUID id, Cuenta user) {
        log.info("Actualizando la cuenta de usuario [" + user.getUsername() + "]");
        Cuenta userTmp = cuentaRepository.findByUuid(id);
        Cuenta userTmpEmail = cuentaRepository.findByUsername(user.getUsername());
        if (userTmpEmail != null && userTmpEmail.getUuid()!= userTmp.getUuid()) {
            throw new RuntimeException("Ya existe otro usuario con el correo electronico especificado.") {
            };
        }
        if (null != userTmp && null != userTmp.getUuid()) {
            user.setUuid(userTmp.getUuid());
            user.setPassword(userTmp.getPassword());
            user = save(user);
            return user;
        } else {
            throw new RuntimeException("La cuenta de usuario con el id " + id + " no fue encontrada.");
        }
    }

    public Boolean updatePassword(UUID id, String password) {
        Cuenta userTmp = cuentaRepository.findByUuid(id);

        if (null != userTmp && null != userTmp.getUuid()) {
            log.info("Actualizando el password de la cuenta de usuario [" + userTmp.getUsername() + "]");
            userTmp.setUuid(userTmp.getUuid());
            userTmp.setPassword(bcryptEncoder.encode(password));
            save(userTmp);
            return true;
        } else {
            throw new RuntimeException("La cuenta de usuario con el id " + id + " no fue encontrada.");
        }
    }

    public void changePassword(String username, String password, String newPassword, String confirmNewPassword) {
        Assert.hasText(password, "Debe especificar la contraseña actual.");
        Assert.hasText(newPassword, "Debe especificar la nueva contraseña.");
        Assert.hasText(confirmNewPassword, "Debe confirmar la nueva contraseña.");
        Assert.isTrue(newPassword.equals(confirmNewPassword), "Las contraseñas no coinciden.");
        Cuenta userTmp = getUserByUserName(username);
        Assert.isTrue(bcryptEncoder.matches(password, userTmp.getPassword()), "Contraseña actual incorrecta.");
        if (null != userTmp && null != userTmp.getUuid()) {
            log.info("Actualizando el password de la cuenta de usuario [" + userTmp.getUsername() + "]");
            userTmp.setUuid(userTmp.getUuid());
            userTmp.setPassword(bcryptEncoder.encode(newPassword));
            save(userTmp);
        } else {
            throw new RuntimeException("La cuenta de usuario " + username + " no fue encontrada.");
        }
    }

    public Iterable<Cuenta> getAll() {
        return cuentaRepository.findAll();
    }

    public Cuenta save(Cuenta user) {
        Cuenta userCreated = cuentaRepository.save(user);
        return userCreated;
    }

    public Iterable<Cuenta> save(Iterable<Cuenta> users) {
        Iterable<Cuenta> usersCreated = cuentaRepository.save(users);
        return usersCreated;
    }

    public void delete(UUID id) {
        Cuenta userTmp = cuentaRepository.findByUuid(id);
        if (null != userTmp && null != userTmp.getUuid()) {
            cuentaRepository.findByUuid(id);
        } else {
            throw new RuntimeException("No existe el usuario con el identificador " + id);
        }
    }

    /**
     * Operacion que realiza la eliminación de una cuenta de usuario,
     *
     * @param usernames
     */
    public void delete(String[] usernames) {
        for (int i = 0; i < usernames.length; i++) {
            Cuenta userTmp = cuentaRepository.findByUsername(usernames[i]);
            if (userTmp != null && userTmp.getUuid() != null) {
                delete(userTmp);
            }
        }
    }

    /**
     * Operacion que realiza la elimiancion de una cuenta de usuarios
     *
     * @param user
     */
    public void delete(Cuenta user) {
        log.debug("Información de la cuenta de ususario a eliminar.");

        cuentaRepository.delete(user.getUuid());
    }

    /**
     * Operacion que realiza la elimincacion de varias cuetnas de usuarios
     *
     * @param users
     */
    public void delete(List<Cuenta> users) {
        cuentaRepository.delete(users);
    }
}
