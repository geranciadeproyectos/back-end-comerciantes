package com.kilangsoft.auth.service;

import com.kilangsoft.auth.repository.RolRepository;
import com.kilangsoft.auth.bo.Rol;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Implementacion del servicio que manejan los Roles.
 *
 * @author Brayan Hamer Rodriguez Sanchez
 */
@Service
public class RoleService {
    private static final Logger log = Logger.getLogger(RoleService.class);

    public static final String ADMINISTRADOR = "ADMINISTRADOR";
    public static final String ADMINISTRADOR_PRINCIPAL = "ADMINISTRADOR PRINCIPAL";

    private RolRepository repository;

    @Autowired
    public RoleService(RolRepository repository) {
        this.repository = repository;
    }

    public List<Rol> getAll() {
        return (List<Rol>) repository.findAll();
    }

    public Rol save(Rol role) {
        log.debug("Informacion del role a crear");
        role = repository.save(role);
        log.debug("Informacion del role creado");
        return role;
    }


    public Rol getByName(String name) {
        return repository.findByNombre(name);
    }


    public List<Rol> save(List<Rol> roles) {
        return (List<Rol>) repository.save(roles);
    }


    public void deleteAll() {
        repository.deleteAll();
    }


    public void delete(String roleName) {
        Rol roleTmp = repository.findByNombre(roleName);
        delete(roleTmp);
    }


    public void delete(String[] roleName) {
        for (int i = 0; i < roleName.length; i++) {
            Rol roleTmp = repository.findByNombre(roleName[i]);
            if (roleTmp != null && roleTmp.getUuid() != null) {
                delete(roleTmp);
            }
        }
    }

    public void delete(Rol role) {
        log.info("Imprimiendo información del role a eliminar");

        repository.delete(role.getUuid());
    }

    public void deletebyIds(List<Rol> roles) {
        log.debug("Imrimiendo información de los roles a eliminar");
        repository.delete(roles);
    }

    public List<Rol> getDefaultsRole(){
        return Arrays.asList(
            new Rol[]{repository.findByNombre(ADMINISTRADOR_PRINCIPAL), repository.findByNombre(ADMINISTRADOR)});
    }
}
