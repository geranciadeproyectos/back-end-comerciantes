package com.kilangsoft.usuario.repository;

import com.kilangsoft.usuario.bo.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario,UUID>{
}
