package com.kilangsoft.config;

import com.kilangsoft.auth.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Clase de configuracion de la seguridad de spring framework, se realiza la
 * configuraciòn de la estructura para la definición de las cuentas de usuario y
 * la configuracion de la estructura para la definición de los roles
 * relacionados a los usuarios. Esta clase es parte del framework de spring
 * security
 *
 * @author Brayan Hamer Rodriguez Sanchez
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfigurationCustom extends WebSecurityConfigurerAdapter {
    private static final Logger log = Logger.getLogger(WebSecurityConfigurationCustom.class);


    private BCryptPasswordEncoder bcryptEncoder;
    private UserService userService;

    @Autowired
    public WebSecurityConfigurationCustom(BCryptPasswordEncoder bcryptEncoder, UserService userService) {
        super();
        this.bcryptEncoder = bcryptEncoder;
        this.userService = userService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info("configuración del administrador de autenticación.");
        auth.userDetailsService(userService).passwordEncoder(bcryptEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("Realizando coniguracion de seguridad  de los  accesos a la urls");
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/comerciantes/api/v1/**").permitAll()
                .anyRequest().fullyAuthenticated()
                .and().httpBasic()
                .and().csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/oauth/token")
                .antMatchers(HttpMethod.OPTIONS, "/comerciantes/api/v1/**")
                .antMatchers(HttpMethod.GET, "/balanceo-de-caja/api/v1/**")
                .antMatchers(HttpMethod.POST, "/balanceo-de-caja/api/v1/**")
                .antMatchers(HttpMethod.PUT, "/balanceo-de-caja/api/v1/**")
                .antMatchers(HttpMethod.DELETE, "/balanceo-de-caja/api/v1/**")
        ;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}