package com.kilangsoft.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class ConfigurationApplicationDefault {
    private static final Logger log = LoggerFactory.getLogger(ConfigurationApplicationDefault.class);
    private static final String RESOURCE_ID = "UN - Comerciantes";

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder mapperBuilder = new Jackson2ObjectMapperBuilder();
        mapperBuilder.indentOutput(true).simpleDateFormat("dd/MM/yyyy");

        SimpleModule module = new SimpleModule();
//        module.addSerializer(LocalTime.class,new CustomJsonLocalTimeSerializer());
//        module.addSerializer(LocalDateTime.class,new CustomJsonLocalDateTimeSerializer());
//        module.addDeserializer(LocalTime.class, new CustomJsonLocalTimeDeserializer());
//        module.addDeserializer(LocalDateTime.class, new CustomJsonLocalDateTimeDeserializer());
        mapperBuilder.build()
                .registerModule(module)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapperBuilder;
    }

    @Bean
    public javax.validation.Validator localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }
}
