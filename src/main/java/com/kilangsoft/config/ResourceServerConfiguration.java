package com.kilangsoft.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    private static final Logger log = LoggerFactory.getLogger(ResourceServerConfiguration.class);
    private static final String RESOURCE_ID = "UN - Comerciantes";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        log.info("Realizando la configuracion del objeto ResourceServerConfiguration ");
        // @formatter:off
        log.info("Asignando la configuracion del Resource_id");
        resources.resourceId(RESOURCE_ID);
        // @formatter:on
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        log.info("Realizando la configuracion de los archivos ");
        log.info("realizando la asignacion de permisos a cada url por el rol o por usuario autenticado");
        http.authorizeRequests()
                //configuracion de acceso al modulo de administracion de cuentas.
                .antMatchers(HttpMethod.GET, "/comerciantes/api/v1/**").permitAll()
                .antMatchers(HttpMethod.POST, "/comerciantes/api/v1/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/comerciantes/api/v1/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/comerciantes/api/v1/**").permitAll()
                .antMatchers(HttpMethod.GET, "/comerciantes/api/v1/oauth2/user/detail").authenticated()
                .antMatchers(HttpMethod.GET, "/comerciantes/api/v1/oauth2/user/info").hasAnyRole("ADMINISTRADOR PRINCIPAL", "ADMINISTRADOR")
                .antMatchers(HttpMethod.GET, "/comerciantes/api/v1/empresa").authenticated()
                .antMatchers(HttpMethod.POST, "/comerciantes/api/v1/empresa").hasAnyRole("ADMINISTRADOR PRINCIPAL", "ADMINISTRADOR")
                .antMatchers(HttpMethod.PUT, "/comerciantes/api/v1/empresa").hasAnyRole("ADMINISTRADOR PRINCIPAL", "ADMINISTRADOR")
                .antMatchers(HttpMethod.DELETE, "/comerciantes/api/v1/empresa").hasAnyRole("ADMINISTRADOR PRINCIPAL", "ADMINISTRADOR")
//                    .antMatchers(HttpMethod.GET, "/geocentrix/api/v1/geocoding/auth2/userdetail").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    .antMatchers(HttpMethod.GET, "/geocentrix/api/v1/geocoding/auth2/users").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    .antMatchers(HttpMethod.POST, "/geocentrix/api/v1/geocoding/auth2/user").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    .antMatchers(HttpMethod.PUT, "/geocentrix/api/v1/geocoding/auth2/user").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    .antMatchers(HttpMethod.PUT, "/geocentrix/api/v1/geocoding/auth2/userPassword").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    .antMatchers(HttpMethod.PUT, "/geocentrix/api/v1/geocoding/auth2/changePassword").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS", "MANAGER_STANDARIZATION", "USER_GEOCODING")
//                    .antMatchers(HttpMethod.DELETE, "/geocentrix/api/v1/geocoding/auth2/user").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    .antMatchers(HttpMethod.GET, "/geocentrix/api/v1/geocoding/auth2/user").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS", "MANAGER_STANDARIZATION", "USER_GEOCODING")
//                    .antMatchers(HttpMethod.GET, "/geocentrix/api/v1/geocoding/auth2/user/*").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS")
//                    //configuracion de acceso a los roles de la aplicacion
//                    .antMatchers(HttpMethod.GET, "/geocentrix/api/v1/geocoding/auth2/roles").hasAnyRole("SUPER_ADMIN", "ADMIN_USERS", "MANAGER_STANDARIZATION", "USER_GEOCODING")
//                    //configuracion de permisos para acceso a la cache
//                    .antMatchers("/geocentrix/api/v1/geocoding/cache/**").hasAnyRole("SUPER_ADMIN", "MANAGER_STANDARIZATION")
//                    //configuracion de permisos para el acceso al modulo de configuracion de parametros de la aplicacion
//                    .antMatchers("/geocentrix/api/v1/geocoding/configuration/**").hasAnyRole("SUPER_ADMIN", "MANAGER_STANDARIZATION")
//                    //configuracion de permisos para el acceso al modulo de configuracion del estandarizador
//                    .antMatchers("/geocentrix/api/v1/geocoding/standarized/config/**").hasAnyRole("SUPER_ADMIN", "MANAGER_STANDARIZATION")
//                    .antMatchers("/geocentrix/api/v1/geocoding/geographic/**").hasAnyRole("SUPER_ADMIN", "USER_GEOCODING")
//                    .antMatchers("/geocentrix/api/v1/geocoding/account/config/loadDefaultUsers").permitAll()
        ;
        // @formatter:on
    }
}