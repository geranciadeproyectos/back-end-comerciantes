package com.kilangsoft.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="comerciantes.path", ignoreUnknownFields = false)
public class ConfiguracionRutasDeAlmacenamiento {

    private String imagenes;

    public String getImagenes() {
        return imagenes;
    }

    public void setImagenes(String imagenes) {
        this.imagenes = imagenes;
    }
}
