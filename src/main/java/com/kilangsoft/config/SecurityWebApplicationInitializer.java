package com.kilangsoft.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Clase que instancia el mecanismo de seguridad del proyecto.
 * 
 * @author Brayan Hamer Rodriguez Sanchez
 *
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

	public SecurityWebApplicationInitializer() {
		super(WebSecurityConfigurationCustom.class);
	}
}