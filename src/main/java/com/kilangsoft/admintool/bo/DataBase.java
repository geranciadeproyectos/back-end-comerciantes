package com.kilangsoft.admintool.bo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kilangsoft.auth.bo.Cuenta;
import com.kilangsoft.empresa.bo.Empresa;
import com.kilangsoft.empresa.bo.TipoEmpresa;
import com.kilangsoft.auth.bo.Rol;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataBase {
    private List<TipoEmpresa> tipoEmpresas;
    private List<Rol> roles;
    private List<Cuenta> cuentas;
    private List<Empresa> empresas;


    public List<TipoEmpresa> getTipoEmpresas() {
        return tipoEmpresas;
    }

    public void setTipoEmpresas(List<TipoEmpresa> tipoEmpresas) {
        this.tipoEmpresas = tipoEmpresas;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public List<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }
}
