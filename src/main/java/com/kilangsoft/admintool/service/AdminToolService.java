package com.kilangsoft.admintool.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.kilangsoft.admintool.bo.DataBase;
import com.kilangsoft.auth.repository.CuentaRepository;
import com.kilangsoft.auth.repository.RolRepository;
import com.kilangsoft.auth.bo.Cuenta;
import com.kilangsoft.auth.service.RoleService;
import com.kilangsoft.empresa.bo.Empresa;
import com.kilangsoft.empresa.bo.TipoEmpresa;
import com.kilangsoft.empresa.repository.EmpresaRepository;
import com.kilangsoft.empresa.repository.TipoDeEmpresaRepository;
import com.kilangsoft.auth.bo.Rol;
import com.kilangsoft.usuario.bo.Usuario;
import com.kilangsoft.usuario.repository.UsuarioRepository;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AdminToolService {
    private static final Logger log = Logger.getLogger(AdminToolService.class);
    private EmpresaRepository empresaRepository;
    private TipoDeEmpresaRepository tipoDeEmpresaRepository;
    private RolRepository rolRepository;
    private UsuarioRepository usuarioRepository;
    private CuentaRepository cuentaRepository;
    private BCryptPasswordEncoder bcryptEncoder;
    private ObjectMapper objectMapper;

    @Autowired
    public AdminToolService(
            EmpresaRepository empresaRepository, TipoDeEmpresaRepository tipoDeEmpresaRepository, RolRepository rolRepository, UsuarioRepository usuarioRepository, CuentaRepository cuentaRepository, BCryptPasswordEncoder bcryptEncoder, ObjectMapper objectMapper) {
        this.empresaRepository = empresaRepository;
        this.tipoDeEmpresaRepository = tipoDeEmpresaRepository;
        this.rolRepository = rolRepository;
        this.usuarioRepository = usuarioRepository;
        this.cuentaRepository = cuentaRepository;
        this.bcryptEncoder = bcryptEncoder;
        this.objectMapper = objectMapper;
    }

    @Transactional
    public void loadDataDefault() {
        empresaRepository.deleteAll();
        cuentaRepository.deleteAll();
        usuarioRepository.deleteAll();
        rolRepository.deleteAll();
        tipoDeEmpresaRepository.deleteAll();

        Cuenta sacastañedaa = new Cuenta("sacastañedaa", bcryptEncoder.encode("123456"), new Usuario("Sergio Alejandro Castañeda Astaiza", "1026261752", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        sacastañedaa = cuentaRepository.save(sacastañedaa);
        Cuenta ljacero = new Cuenta("ljacero", bcryptEncoder.encode("123456"), new Usuario("Luis José Acero ", "1445854741", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        ljacero = cuentaRepository.save(ljacero);
        Cuenta agarcia = new Cuenta("agarcia", bcryptEncoder.encode("123456"), new Usuario("Alejandro  García ", "1034963214", rolRepository.findByNombre("CONSULTA") == null ? rolRepository.save(new Rol("CONSULTA")) : rolRepository.findByNombre("CONSULTA")));
        agarcia = cuentaRepository.save(agarcia);
        Cuenta faapolinarc = new Cuenta("faapolinarc", bcryptEncoder.encode("123456"), new Usuario("Fernando Augusto Apolinar Caraballo", "10236589", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        faapolinarc = cuentaRepository.save(faapolinarc);
        Cuenta amapontem = new Cuenta("amapontem", bcryptEncoder.encode("123456"), new Usuario("Ana Maria Aponte Melo", "1018422036", rolRepository.findByNombre(RoleService.ADMINISTRADOR) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)));
        amapontem = cuentaRepository.save(amapontem);
        Cuenta jvgarciag = new Cuenta("jvgarciag", bcryptEncoder.encode("123456"), new Usuario("Jenny Viviana García Güiza", "15269384", rolRepository.findByNombre("CONSULTA") == null ? rolRepository.save(new Rol("CONSULTA")) : rolRepository.findByNombre("CONSULTA")));
        jvgarciag = cuentaRepository.save(jvgarciag);
        Cuenta ebarrerar = new Cuenta("ebarrerar", bcryptEncoder.encode("123456"), new Usuario("Edgar  Barrera Ríos", "1032403447", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        ebarrerar = cuentaRepository.save(ebarrerar);
        Cuenta pespinosac = new Cuenta("pespinosac", bcryptEncoder.encode("123456"), new Usuario("Paola  Espinosa Cynthia", "302408317", rolRepository.findByNombre("ROL 245") == null ? rolRepository.save(new Rol("ROL 245")) : rolRepository.findByNombre("ROL 245")));
        pespinosac = cuentaRepository.save(pespinosac);
        Cuenta tfarot = new Cuenta("tfarot", bcryptEncoder.encode("123456"), new Usuario("Tatiana  Faro Téllez", "103526984", rolRepository.findByNombre("ROL 592") == null ? rolRepository.save(new Rol("ROL 592")) : rolRepository.findByNombre("ROL 592")));
        tfarot = cuentaRepository.save(tfarot);
        Cuenta oafeoc = new Cuenta("oafeoc", bcryptEncoder.encode("123456"), new Usuario("Omar Andrés Feo Cárdenas", "1058450123", rolRepository.findByNombre("ROL 888") == null ? rolRepository.save(new Rol("ROL 888")) : rolRepository.findByNombre("ROL 888")));
        oafeoc = cuentaRepository.save(oafeoc);
        Cuenta cjbenavidesp = new Cuenta("cjbenavidesp", bcryptEncoder.encode("123456"), new Usuario("Christian Javier Benavides Prada", "7450638", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        cjbenavidesp = cuentaRepository.save(cjbenavidesp);
        Cuenta dcbenitezp = new Cuenta("dcbenitezp", bcryptEncoder.encode("123456"), new Usuario("Diana Carolina Benitez Prada", "1013568971", rolRepository.findByNombre(RoleService.ADMINISTRADOR) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)));
        dcbenitezp = cuentaRepository.save(dcbenitezp);
        Cuenta cabernalb = new Cuenta("cabernalb", bcryptEncoder.encode("123456"), new Usuario("Carlos Arturo Bernal Benavides", "19228456", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        cabernalb = cuentaRepository.save(cabernalb);
        Cuenta abertran = new Cuenta("abertran", bcryptEncoder.encode("123456"), new Usuario("Ariel  Bertrán ", "135874596", rolRepository.findByNombre(RoleService.ADMINISTRADOR) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)));
        abertran = cuentaRepository.save(abertran);
        Cuenta jmbetancourt = new Cuenta("jmbetancourt", bcryptEncoder.encode("123456"), new Usuario("Juan Manuel Betancourt ", "1204457484", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        jmbetancourt = cuentaRepository.save(jmbetancourt);
        Cuenta cacabreral = new Cuenta("cacabreral", bcryptEncoder.encode("123456"), new Usuario("Cesar Augusto Cabrera Lozano", "91785632", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        cacabreral = cuentaRepository.save(cacabreral);
        Cuenta facadenam = new Cuenta("facadenam", bcryptEncoder.encode("123456"), new Usuario("Felipe Andrés Cadena Molano", "56897412", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        facadenam = cuentaRepository.save(facadenam);
        Cuenta ccaicedoc = new Cuenta("ccaicedoc", bcryptEncoder.encode("123456"), new Usuario("Carlos  Caicedo Contreras", "78963413", rolRepository.findByNombre(RoleService.ADMINISTRADOR) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)));
        ccaicedoc = cuentaRepository.save(ccaicedoc);
        Cuenta oagarciap = new Cuenta("oagarciap", bcryptEncoder.encode("123456"), new Usuario("Omar Andrés García Pérez", "14568710", rolRepository.findByNombre("CONSULTA") == null ? rolRepository.save(new Rol("CONSULTA")) : rolRepository.findByNombre("CONSULTA")));
        oagarciap = cuentaRepository.save(oagarciap);
        Cuenta rcalaverac = new Cuenta("rcalaverac", bcryptEncoder.encode("123456"), new Usuario("Román  Calavera Calvo", "77673710", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        rcalaverac = cuentaRepository.save(rcalaverac);
        Cuenta jacalderon = new Cuenta("jacalderon", bcryptEncoder.encode("123456"), new Usuario("José Andrés Calderón ", "5237842", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        jacalderon = cuentaRepository.save(jacalderon);
        Cuenta ccamachoc = new Cuenta("ccamachoc", bcryptEncoder.encode("123456"), new Usuario("Camilo  Camacho Camargo", "79856245", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        ccamachoc = cuentaRepository.save(ccamachoc);
        Cuenta macamargol = new Cuenta("macamargol", bcryptEncoder.encode("123456"), new Usuario("María Andrea Camargo López", "91011063", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        macamargol = cuentaRepository.save(macamargol);
        Cuenta acaminog = new Cuenta("acaminog", bcryptEncoder.encode("123456"), new Usuario("Alberto  Camino Grande", "18880944", rolRepository.findByNombre(RoleService.ADMINISTRADOR) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)));
        acaminog = cuentaRepository.save(acaminog);
        Cuenta ajdazao = new Cuenta("ajdazao", bcryptEncoder.encode("123456"), new Usuario("Andrés Javier Daza Ortega", "5566332", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        ajdazao = cuentaRepository.save(ajdazao);
        Cuenta cddiazo = new Cuenta("cddiazo", bcryptEncoder.encode("123456"), new Usuario("Christian David Díaz Oviedo", "1023564893", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        cddiazo = cuentaRepository.save(cddiazo);
        Cuenta ladiazp = new Cuenta("ladiazp", bcryptEncoder.encode("123456"), new Usuario("Luis Alberto Diaz Patiño", "15269380", rolRepository.findByNombre(RoleService.ADMINISTRADOR) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)));
        ladiazp = cuentaRepository.save(ladiazp);
        Cuenta ltdiazs = new Cuenta("ltdiazs", bcryptEncoder.encode("123456"), new Usuario("Leidy Tatiana Díaz Silva", "51225258", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        ltdiazs = cuentaRepository.save(ltdiazs);
        Cuenta ragarcia = new Cuenta("ragarcia", bcryptEncoder.encode("123456"), new Usuario("Rodolfo Andrés García ", "3024083178", rolRepository.findByNombre("CONSULTA") == null ? rolRepository.save(new Rol("CONSULTA")) : rolRepository.findByNombre("CONSULTA")));
        ragarcia = cuentaRepository.save(ragarcia);
        Cuenta jcgarciav = new Cuenta("jcgarciav", bcryptEncoder.encode("123456"), new Usuario("Juan Camilo García Vaquero", "1012365897", rolRepository.findByNombre("CONSULTA") == null ? rolRepository.save(new Rol("CONSULTA")) : rolRepository.findByNombre("CONSULTA")));
        jcgarciav = cuentaRepository.save(jcgarciav);
        Cuenta jfdominguezr = new Cuenta("jfdominguezr", bcryptEncoder.encode("123456"), new Usuario("Juan Felipe Domínguez Rojas", "1015287966", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        jfdominguezr = cuentaRepository.save(jfdominguezr);
        Cuenta faduranb = new Cuenta("faduranb", bcryptEncoder.encode("123456"), new Usuario("Felipe Antonio Durán Buitrago", "102369874", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        faduranb = cuentaRepository.save(faduranb);
        Cuenta sduranc = new Cuenta("sduranc", bcryptEncoder.encode("123456"), new Usuario("Santiago  Durán Casas", "1025365987", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        sduranc = cuentaRepository.save(sduranc);
        Cuenta aeduranm = new Cuenta("aeduranm", bcryptEncoder.encode("123456"), new Usuario("Arly Ernesto Duran Meluk", "15269386", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        aeduranm = cuentaRepository.save(aeduranm);
        Cuenta ddussan = new Cuenta("ddussan", bcryptEncoder.encode("123456"), new Usuario("David  Dussan ", "256985412", rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL)));
        ddussan = cuentaRepository.save(ddussan);

        Empresa empresa0 = new Empresa("Sistemas y Administración S.A.", "40375896", tipoDeEmpresaRepository.findByNombre("SISTEMA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("SISTEMA", 0)) : tipoDeEmpresaRepository.findByNombre("SISTEMA"), Arrays.asList(new Usuario[]{sacastañedaa.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa0 = empresaRepository.save(empresa0);
        Empresa empresa1 = new Empresa("Adolfo González Ltda.", "85693746 ", tipoDeEmpresaRepository.findByNombre("DISTRIBUIDOR") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("DISTRIBUIDOR", 1)) : tipoDeEmpresaRepository.findByNombre("DISTRIBUIDOR"), Arrays.asList(new Usuario[]{ljacero.getUsuario(), agarcia.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa1 = empresaRepository.save(empresa1);
        Empresa empresa2 = new Empresa("Alejandro Bautista S.A.", "10180358", tipoDeEmpresaRepository.findByNombre("DISTRIBUIDOR") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("DISTRIBUIDOR", 1)) : tipoDeEmpresaRepository.findByNombre("DISTRIBUIDOR"), Arrays.asList(new Usuario[]{faapolinarc.getUsuario(), amapontem.getUsuario(), jvgarciag.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa2 = empresaRepository.save(empresa2);
        Empresa empresa3 = new Empresa("Belén Milena Orejuela Jara Ltda.", "40859774", tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("SUBDISTRIBUIDOR", 2)) : tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR"), Arrays.asList(new Usuario[]{ebarrerar.getUsuario(), pespinosac.getUsuario(), tfarot.getUsuario(), oafeoc.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa3 = empresaRepository.save(empresa3);
        Empresa empresa4 = new Empresa("Buenos Aires Colombia S.A.", "1121342139", tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("SUBDISTRIBUIDOR", 2)) : tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR"), Arrays.asList(new Usuario[]{cjbenavidesp.getUsuario(), dcbenitezp.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa4 = empresaRepository.save(empresa4);
        Empresa empresa5 = new Empresa("Blanca Rosa Pulgarín S.A.", "27658935", tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("SUBDISTRIBUIDOR", 2)) : tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR"), Arrays.asList(new Usuario[]{cabernalb.getUsuario(), abertran.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa5 = empresaRepository.save(empresa5);
        Empresa empresa6 = new Empresa("Bryan Sopó Ltda.", "1012654357", tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("SUBDISTRIBUIDOR", 2)) : tipoDeEmpresaRepository.findByNombre("SUBDISTRIBUIDOR"), Arrays.asList(new Usuario[]{jmbetancourt.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa6 = empresaRepository.save(empresa6);
        Empresa empresa7 = new Empresa("Camilo Abella Ltda.", "18035717", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), Arrays.asList(new Usuario[]{cacabreral.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa7 = empresaRepository.save(empresa7);
        Empresa empresa8 = new Empresa("Camilo Alberto Cangrejo Beltrán S.A.", "1024235674", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), Arrays.asList(new Usuario[]{facadenam.getUsuario(), ccaicedoc.getUsuario(), oagarciap.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa8 = empresaRepository.save(empresa8);
        Empresa empresa9 = new Empresa("Camilo Ariza Ltda.", "18035723", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), Arrays.asList(new Usuario[]{rcalaverac.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa9 = empresaRepository.save(empresa9);
        Empresa empresa10 = new Empresa("Camilo Uribe Ltda.", "72112331", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa10 = empresaRepository.save(empresa10);
        Empresa empresa11 = new Empresa("Carlos Arturo Chasoy Ltda.", "1120394374", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), Arrays.asList(new Usuario[]{jacalderon.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa11 = empresaRepository.save(empresa11);
        Empresa empresa12 = new Empresa("Carmenza Umaña Ltda.", "65477213", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa12 = empresaRepository.save(empresa12);
        Empresa empresa13 = new Empresa("Cesar Humberto Ortiz Ltda.", "122991257", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), Arrays.asList(new Usuario[]{ccamachoc.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa13 = empresaRepository.save(empresa13);
        Empresa empresa14 = new Empresa("Cristina Nieto S.A.", "45698912", tipoDeEmpresaRepository.findByNombre("CANAL") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("CANAL", 3)) : tipoDeEmpresaRepository.findByNombre("CANAL"), Arrays.asList(new Usuario[]{macamargol.getUsuario(), acaminog.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa14 = empresaRepository.save(empresa14);
        Empresa empresa15 = new Empresa("David Camilo Toro Murillo Ltda.", "109758614", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{ajdazao.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa15 = empresaRepository.save(empresa15);
        Empresa empresa16 = new Empresa("Diana Alexandra Pérez Ltda.", "1121465879", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{cddiazo.getUsuario(), ladiazp.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa16 = empresaRepository.save(empresa16);
        Empresa empresa17 = new Empresa("Diana Angarita Ltda.", "18035741", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{ltdiazs.getUsuario(), ragarcia.getUsuario(), jcgarciav.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa17 = empresaRepository.save(empresa17);
        Empresa empresa18 = new Empresa("Diana Cristina Cristancho Ltda.", "2569891", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{jfdominguezr.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa18 = empresaRepository.save(empresa18);
        Empresa empresa19 = new Empresa("Diego Arroyabe Ltda.", "18035753", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{faduranb.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa19 = empresaRepository.save(empresa19);
        Empresa empresa20 = new Empresa("Domingo Zárate Ltda.", "98135442", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{sduranc.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa20 = empresaRepository.save(empresa20);
        Empresa empresa21 = new Empresa("Daniel Morona Ltda.", "1032433442", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa21 = empresaRepository.save(empresa21);
        Empresa empresa22 = new Empresa("Demetrio Rodríguez Guzmán Ltda.", "101985574", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa22 = empresaRepository.save(empresa22);
        Empresa empresa23 = new Empresa("Diógenes Fernández García Ltda.", "1124566390", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{aeduranm.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa23 = empresaRepository.save(empresa23);
        Empresa empresa24 = new Empresa("Doris Marcela Vásquez Gil Ltda.", "79584236", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), Arrays.asList(new Usuario[]{ddussan.getUsuario()}), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa24 = empresaRepository.save(empresa24);
        Empresa empresa25 = new Empresa("David Orlando Gutiérrez Guevara Ltda.", "12348755", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa25 = empresaRepository.save(empresa25);
        Empresa empresa26 = new Empresa("Dionisio Romero Ltda.", "34218660", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa26 = empresaRepository.save(empresa26);
        Empresa empresa27 = new Empresa("Dora Carranza Ltda.", "54326784", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa27 = empresaRepository.save(empresa27);
        Empresa empresa28 = new Empresa("Danny Morales Ltda.", "97845211", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa28 = empresaRepository.save(empresa28);
        Empresa empresa29 = new Empresa("Dagoberto Baranda Ltda.", "46987389", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa29 = empresaRepository.save(empresa29);
        Empresa empresa30 = new Empresa("Dianita Vera Ávila Ltda.", "1014785586", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa30 = empresaRepository.save(empresa30);
        Empresa empresa31 = new Empresa("Douglas Sepúlveda Ltda.", "85465125", tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA") == null ? tipoDeEmpresaRepository.save(new TipoEmpresa("PUNTO DE VENTA", 4)) : tipoDeEmpresaRepository.findByNombre("PUNTO DE VENTA"), new ArrayList<>(), Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)}));
        empresa31 = empresaRepository.save(empresa31);

        List<Empresa> empresas = new ArrayList<>();
        empresa1.setEmpresaPadre(empresaRepository.findByNit("40375896"));
        empresas.add(empresa1);
        empresa2.setEmpresaPadre(empresaRepository.findByNit("40375896"));
        empresas.add(empresa2);
        empresa3.setEmpresaPadre(empresaRepository.findByNit("85693746 "));
        empresas.add(empresa3);
        empresa4.setEmpresaPadre(empresaRepository.findByNit("85693746 "));
        empresas.add(empresa4);
        empresa5.setEmpresaPadre(empresaRepository.findByNit("10180358"));
        empresas.add(empresa5);
        empresa6.setEmpresaPadre(empresaRepository.findByNit("10180358"));
        empresas.add(empresa6);
        empresa7.setEmpresaPadre(empresaRepository.findByNit("40859774"));
        empresas.add(empresa7);
        empresa8.setEmpresaPadre(empresaRepository.findByNit("40859774"));
        empresas.add(empresa8);
        empresa9.setEmpresaPadre(empresaRepository.findByNit("1121342139"));
        empresas.add(empresa9);
        empresa10.setEmpresaPadre(empresaRepository.findByNit("1121342139"));
        empresas.add(empresa10);
        empresa11.setEmpresaPadre(empresaRepository.findByNit("27658935"));
        empresas.add(empresa11);
        empresa12.setEmpresaPadre(empresaRepository.findByNit("27658935"));
        empresas.add(empresa12);
        empresa13.setEmpresaPadre(empresaRepository.findByNit("1012654357"));
        empresas.add(empresa13);
        empresa14.setEmpresaPadre(empresaRepository.findByNit("1012654357"));
        empresas.add(empresa14);
        empresa15.setEmpresaPadre(empresaRepository.findByNit("18035717"));
        empresas.add(empresa15);
        empresa16.setEmpresaPadre(empresaRepository.findByNit("18035717"));
        empresas.add(empresa16);
        empresa17.setEmpresaPadre(empresaRepository.findByNit("1024235674"));
        empresas.add(empresa17);
        empresa18.setEmpresaPadre(empresaRepository.findByNit("1024235674"));
        empresas.add(empresa18);
        empresa19.setEmpresaPadre(empresaRepository.findByNit("18035723"));
        empresas.add(empresa19);
        empresa20.setEmpresaPadre(empresaRepository.findByNit("18035723"));
        empresas.add(empresa20);
        empresa21.setEmpresaPadre(empresaRepository.findByNit("72112331"));
        empresas.add(empresa21);
        empresa22.setEmpresaPadre(empresaRepository.findByNit("72112331"));
        empresas.add(empresa22);
        empresa23.setEmpresaPadre(empresaRepository.findByNit("1120394374"));
        empresas.add(empresa23);
        empresa24.setEmpresaPadre(empresaRepository.findByNit("1120394374"));
        empresas.add(empresa24);
        empresa25.setEmpresaPadre(empresaRepository.findByNit("65477213"));
        empresas.add(empresa25);
        empresa26.setEmpresaPadre(empresaRepository.findByNit("65477213"));
        empresas.add(empresa26);
        empresa27.setEmpresaPadre(empresaRepository.findByNit("122991257"));
        empresas.add(empresa27);
        empresa28.setEmpresaPadre(empresaRepository.findByNit("122991257"));
        empresas.add(empresa28);
        empresa29.setEmpresaPadre(empresaRepository.findByNit("45698912"));
        empresas.add(empresa29);
        empresa30.setEmpresaPadre(empresaRepository.findByNit("45698912"));
        empresas.add(empresa30);
        empresa31.setEmpresaPadre(empresaRepository.findByNit("45698912"));
        empresas.add(empresa31);

        List<Rol> rolesDefault = Arrays.asList(new Rol[]{rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR_PRINCIPAL)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL), rolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? rolRepository.save(new Rol(RoleService.ADMINISTRADOR)) : rolRepository.findByNombre(RoleService.ADMINISTRADOR)});
        for (Empresa empresa : empresas) {
            List<Rol> roles = new ArrayList<>();
            roles.addAll(rolesDefault);
            if (empresa.getUsuarios() != null) {
                for (Usuario usuario : empresa.getUsuarios()) {
                    Rol rolTmp = usuario.getRol();
                    if (rolTmp != null) {
                        boolean found = false;
                        for (int i = 0; i < roles.size(); i++) {
                            if (roles.get(i).getNombre().equals(rolTmp.getNombre())) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            roles.add(rolTmp);
                        }
                    }
                }
            }
            empresa.setRoles(roles);
        }

        // empresaRepository.save(empresas);
    }

    @Transactional
    public DataBase upload(InputStream inputStream) {
        DataBase database = new DataBase();
        try {
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, StandardCharsets.UTF_8);
            String content = writer.toString();
            database = objectMapper.readValue(content, new TypeReference<DataBase>() {
            });

            rolRepository.deleteAll();
            usuarioRepository.deleteAll();
            tipoDeEmpresaRepository.deleteAll();
            cuentaRepository.deleteAll();
            empresaRepository.deleteAll();

            rolRepository.save(database.getRoles());
            tipoDeEmpresaRepository.save(database.getTipoEmpresas());

            database.getCuentas().forEach(cuenta -> {
                Rol rolTmp = rolRepository.findByNombre(cuenta.getUsuario().getRol().getNombre()) == null ? rolRepository.save(cuenta.getUsuario().getRol()) : rolRepository.findByNombre(cuenta.getUsuario().getRol().getNombre());
                cuenta.getUsuario().setRol(rolTmp);
            });
            cuentaRepository.save(database.getCuentas());


            List<Empresa> empresasTmp = new ArrayList<>();
            for (Empresa empresa : database.getEmpresas()) {
                TipoEmpresa tipoEmpresaTmp = tipoDeEmpresaRepository.findByNombre(empresa.getTipo().getNombre()) == null ? tipoDeEmpresaRepository.save(empresa.getTipo()) : tipoDeEmpresaRepository.findByNombre(empresa.getTipo().getNombre());

                List<Rol> rolesTmp = new ArrayList<>();
                empresa.getRoles().forEach(rol -> {
                    Rol rolTmp = rolRepository.findByNombre(rol.getNombre()) == null ? rolRepository.save(rol) : rolRepository.findByNombre(rol.getNombre());
                    rolesTmp.add(rolTmp);
                });

                List<Usuario> usuariosTmp = new ArrayList<>();
                empresa.getUsuarios().forEach(usuario -> {
                    Usuario usuarioTmp = cuentaRepository.findByUsuarioCedula(usuario.getCedula()).getUsuario();
                    usuariosTmp.add(usuarioTmp);
                });

                Empresa empresaNew = new Empresa();
                empresaNew.setNombre(empresa.getNombre());
                empresaNew.setNit(empresa.getNit());
                empresaNew.setTelefono(empresa.getTelefono());
                empresaNew.setTipo(tipoEmpresaTmp);
                empresaNew.setRoles(rolesTmp);
                empresaNew.setUsuarios(usuariosTmp);
                empresaNew.setLogo(empresa.getLogo());
                empresasTmp.add(empresaNew);
            }
            empresasTmp = empresaRepository.save(empresasTmp);

            for (Empresa empresa : database.getEmpresas()) {
                if (empresa.getEmpresaPadre() != null) {
                    Empresa empresaPadreTmp = empresaRepository.findByNit(empresa.getEmpresaPadre().getNit());
                    Empresa empresaTmp = empresaRepository.findByNit(empresa.getNit());
                    empresaTmp.setEmpresaPadre(empresaPadreTmp);
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return database;
    }


    public InputStream download() {
        DataBase database = new DataBase();
        database.setTipoEmpresas(Lists.newArrayList(tipoDeEmpresaRepository.findAll()));
        database.getTipoEmpresas().forEach(tipoDeEmpresa -> {
            tipoDeEmpresa.setUuid(null);
        });
        database.setRoles(Lists.newArrayList(rolRepository.findAll()));
        database.getRoles().forEach(rol -> {
            rol.setUuid(null);
        });

        database.setCuentas(Lists.newArrayList(cuentaRepository.findAll()));
        database.getCuentas().forEach(cuenta -> {
            cuenta.setUuid(null);
            cuenta.getUsuario().setUuid(null);
        });

        database.setEmpresas(Lists.newArrayList(empresaRepository.findAll()));
        database.getEmpresas().forEach(empresa -> {

//            empresa.setUuid(null);
            empresa.getTipo().setUuid(null);
            empresa.getRoles().forEach(rolEmpresa -> {
//                rolEmpresa.setUuid(null);
            });
            empresa.getUsuarios().forEach(usuarioEmpresa -> {
//                usuarioEmpresa.setUuid(null);
//                usuarioEmpresa.getRol().setUuid(null);
            });
            if (empresa.getEmpresaPadre() != null) {
                Empresa empresaPadreTmp = new Empresa();
                empresaPadreTmp.setNit(empresa.getEmpresaPadre().getNit());
                empresa.setEmpresaPadre(empresaPadreTmp);
            }
        });


        try {
            String content = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(database);
            InputStream stream = new ByteArrayInputStream(content.toString().getBytes(StandardCharsets.UTF_8));
            return stream;
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            throw new RuntimeException("No fue posible generar el archivo Json para la descarga.intente mas tarde");
        }
    }


}