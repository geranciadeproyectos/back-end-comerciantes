package com.kilangsoft.admintool.controller;

import com.kilangsoft.admintool.bo.DataBase;
import com.kilangsoft.admintool.service.AdminToolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

@RestController
@RequestMapping(value = {"comerciantes/api/v1/admin/"}, produces = {"application/json"})
@Api(value = "/admintool", description = "Operaciones Relacionadas con la gestión del sistema", consumes = "application/json")
public class AdminToolController {
    private static final Logger log = Logger.getLogger(AdminToolController.class);
    private AdminToolService adminToolService;


    @Autowired
    public AdminToolController(AdminToolService adminToolService) {
        this.adminToolService = adminToolService;
    }


    @ApiOperation(value = "Crea los registros por default para el funcionamiento de la aplicación", notes = "Realiza la creacion de roles y tipo de empresa por defecto", httpMethod = "GET")
    @ApiResponses({@ApiResponse(code = 200, message = "Detalles del usuario", response = UserDetails.class)})
    @RequestMapping(value = "/loadDataDefault", method = {RequestMethod.GET}, produces = {"application/json"})
    public void loadDataDefault() {
        adminToolService.loadDataDefault();
    }

    @RequestMapping(value = "/restore", method = RequestMethod.POST)
    @ResponseBody
    public DataBase upload(@RequestParam("file") MultipartFile file) {
        log.debug("Realizando carga masiva de oficinas");
        log.debug("Nombre Original : " + file.getOriginalFilename());
        log.debug("Nombre : " + file.getName());
        log.debug("Tipo de contenido : " + file.getContentType());
        log.debug("Tamaño : " + file.getSize());

        if (file.isEmpty()) {
            throw new RuntimeException("El archivo no tiene contenido para la carga de información");
        }
        InputStream inputStream = null;
        DataBase database = null;
        try {
            byte[] content = file.getBytes();
            log.info(String.valueOf(file.isEmpty()));
            inputStream = new ByteArrayInputStream(content);
            log.info("Validando el tipo de archivo cargado");
            String[] fileInfo = file.getOriginalFilename().split(Pattern.quote("."));
            if (fileInfo.length >= 2) {
                if (fileInfo[fileInfo.length - 1].toUpperCase().equalsIgnoreCase("JSON")) {
                    log.info("Realizando carga de las oficinas por archivo en formato JSON");
                    database = adminToolService.upload(inputStream);
                    return database;
                }else {
                    throw new RuntimeException("solo es valido restaura con archivos en formato json");
                }
            } else {
                log.error("No es posible porcesar el archivo no se conoce la extensión del archivo [" + file.getOriginalFilename() + "]");
                throw new RuntimeException("No es posible porcesar el archivo no se conoce la extensión del archivo [" + file.getOriginalFilename() + "]. solo es valido los archivos en formato json");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Ocurrio un error en la lectura del archivo");
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (Exception ex) {

            }
        }
    }

    @RequestMapping(value = "/backup", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response) {

        LocalDate fecha = LocalDate.now();
        fecha.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"backup_"+fecha.toString()+".json\"");
            response.setHeader("Content-Type", "application/json");
            response.setHeader("Content-Transfer-Encoding", "binary");
            InputStream stream = adminToolService.download();
            org.apache.commons.io.IOUtils.copy(stream, response.getOutputStream());
            response.flushBuffer();

        } catch (IOException ex) {
            log.info("Error escribiendo el archivo en el flujo de salida del servicio. nombre del archivo es 'backup_"+fecha.toString()+".json' ", ex);
            throw new RuntimeException("Error escribiendo el archivo en el flujo de salida del servicio");
        }

    }
}
