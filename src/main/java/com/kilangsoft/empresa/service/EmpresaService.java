package com.kilangsoft.empresa.service;

import com.kilangsoft.auth.repository.CuentaRepository;
import com.kilangsoft.auth.repository.RolRepository;
import com.kilangsoft.auth.service.RoleService;
import com.kilangsoft.config.properties.ConfiguracionRutasDeAlmacenamiento;
import com.kilangsoft.auth.bo.Cuenta;
import com.kilangsoft.empresa.bo.Empresa;
import com.kilangsoft.empresa.bo.TipoEmpresa;
import com.kilangsoft.empresa.repository.EmpresaRepository;
import com.kilangsoft.empresa.repository.TipoDeEmpresaRepository;
import com.kilangsoft.auth.bo.Rol;
import com.kilangsoft.usuario.bo.Usuario;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class EmpresaService {
    private static final Logger log = Logger.getLogger(EmpresaService.class);

    private EmpresaRepository empresaRepository;
    private CuentaRepository cuentaRepository;
    private RolRepository roleRolRepository;
    private TipoDeEmpresaRepository tipoDeEmpresaRepository;
    private ConfiguracionRutasDeAlmacenamiento configuracionRutasDeAlmacenamiento;
    private BCryptPasswordEncoder bcryptEncoder;

    @Autowired
    public EmpresaService(EmpresaRepository empresaRepository, CuentaRepository cuentaRepository, RolRepository roleRolRepository, TipoDeEmpresaRepository tipoDeEmpresaRepository, ConfiguracionRutasDeAlmacenamiento configuracionRutasDeAlmacenamiento, BCryptPasswordEncoder bcryptEncoder) {
        this.empresaRepository = empresaRepository;
        this.cuentaRepository = cuentaRepository;
        this.roleRolRepository = roleRolRepository;
        this.tipoDeEmpresaRepository = tipoDeEmpresaRepository;
        this.configuracionRutasDeAlmacenamiento = configuracionRutasDeAlmacenamiento;
        this.bcryptEncoder = bcryptEncoder;
    }

    @Transactional
    public Empresa getEmpresa(String username) {
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        return empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
    }

    @Transactional
    public Empresa createEmpresa(String username, Empresa empresa, MultipartFile file) {
        validateAndCreateFolders();
        validateExtensionFileFile(file);
        validateEmpresa(empresa, true,true);
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresaUsuario = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
        TipoEmpresa tipo = getNivelEmpresasAdministradas(username);
        if (tipo == null) {
            throw new RuntimeException("Su nivel de empresa no tiene la opcion de crear empresas en niveles inferiores de la jerarquia");
        } else {
            empresa.setEmpresaPadre(empresaUsuario);
            empresa.setTipo(tipo);

            if (file != null) {
                log.info(file);
                log.info("Realizando carga masiva de oficinas");
                log.info("Nombre Original : " + file.getOriginalFilename());
                log.info("Nombre : " + file.getName());
                log.info("Tipo de contenido : " + file.getContentType());
                log.info("Tamaño : " + file.getSize());
                log.info("Almacenando log");
                String nameLogo = empresa.getNit() + "_" + empresa.getNombre() + "." + file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
                empresa.setLogo(nameLogo);
                File convFile = new File(configuracionRutasDeAlmacenamiento.getImagenes() + "\\" + nameLogo);
                log.info(convFile.getAbsolutePath());
                log.info(convFile.getName());
                try {
                    file.transferTo(convFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("No fue posible almacenar el logo de la empresa intente de nuevo");
                }
            }
            return empresa = empresaRepository.save(empresa);
        }
    }

    @Transactional
    public Empresa updateEmpresa(String username, Empresa empresa, MultipartFile file) {
        validateExtensionFileFile(file);
        validateAndCreateFolders();
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresaTmp = empresaRepository.findOne(empresa.getUuid());

        if (!empresaTmp.getNombre().equals(empresa.getNombre())) {
            validateEmpresa(empresa, true, false);
            empresaTmp.setNombre(empresa.getNombre());
        }
        if (!empresaTmp.getNit().equals(empresa.getNit())) {
            validateEmpresa(empresa, false, true);
            empresaTmp.setNit(empresa.getNit());
        }

        empresaTmp.setTelefono(empresa.getTelefono());


        if ((!empresa.getNombre().equals(empresaTmp.getNombre()) || !empresa.getNit().equals(empresaTmp.getNit())) && file == null && empresaTmp.getLogo() != null) {
            //Renombra el archivo con los cambios de los datos de la empresa
            String nameLogoTmp = empresaTmp.getLogo();
            String nameNewLogo = empresa.getNit() + "_" + empresa.getNombre() + "." + nameLogoTmp.split("\\.")[nameLogoTmp.split("\\.").length - 1];
            log.info("path anterior" + nameLogoTmp);
            log.info("path nuevo" + nameNewLogo);
            try {
                FileUtils.moveFile(
                        FileUtils.getFile(configuracionRutasDeAlmacenamiento.getImagenes() + "\\" + nameLogoTmp),
                        FileUtils.getFile(configuracionRutasDeAlmacenamiento.getImagenes() + "\\" + nameNewLogo)
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
            empresaTmp.setLogo(nameNewLogo);
        }
        if (file != null) {
            if (empresaTmp.getLogo() != null) {
                String nameLogoTmp = empresaTmp.getLogo();
                log.info("path a elimianr" + nameLogoTmp);
                File fileDelete = new File(configuracionRutasDeAlmacenamiento.getImagenes() + "\\" + nameLogoTmp);
                fileDelete.delete();
            }

            String nameNewLogo = empresa.getNit() + "_" + empresa.getNombre() + "." + file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
            log.info("patch a actualizar" + nameNewLogo);

            File newFile = new File(configuracionRutasDeAlmacenamiento.getImagenes() + "\\" + nameNewLogo);
            empresaTmp.setLogo(nameNewLogo);
            try {
                file.transferTo(newFile);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("No fue posible almacenar el logo de la empresa intente de nuevo");
            }
        }
        return empresa = empresaRepository.saveAndFlush(empresaTmp);
    }

    public void deleteEmpresa(String name, UUID id) {
        Empresa empresa = empresaRepository.findOne(id);
        List<Empresa> empresasJerarquia = new ArrayList<>();
        empresasJerarquia.add(empresa);

        int i = 0;
        do {
            empresasJerarquia.addAll(empresaRepository.findByEmpresaPadreNitOrderByNombreAsc(empresasJerarquia.get(i).getNit()));
            i++;
        } while (i < empresasJerarquia.size());

        List<Usuario> usuarios = new ArrayList<>();
        empresasJerarquia.forEach(empresaTmp -> {
            usuarios.addAll(empresaTmp.getUsuarios());
            empresaTmp.setEmpresaPadre(null);
            empresaTmp.setRoles(null);
            empresaTmp.setUsuarios(null);
        });

        List<Cuenta> cuentas = new ArrayList<>();
        usuarios.forEach(usuario -> {
            cuentas.add(cuentaRepository.findByUsuarioUuid(usuario.getUuid()));
        });

        empresaRepository.save(empresasJerarquia);
        empresaRepository.delete(empresasJerarquia);
        cuentaRepository.delete(cuentas);
    }

    @Transactional
    public Cuenta getUsuarioAdministrador(String username, UUID uuid) {
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findOne(uuid);
        if (empresa == null) {
            throw new RuntimeException("La empresa con el id " + uuid + toString() + " no existe.");
        }
        final boolean[] esAdministrador = {false};
        empresa.getEmpresaPadre().getUsuarios().forEach(usuario -> {
            if (usuario.getUuid() == cuenta.getUsuario().getUuid()) {
                esAdministrador[0] = true;
            }
        });
        if (esAdministrador[0]) {
            final Cuenta[] cuentaUsuarioAdministrador = new Cuenta[1];
            empresa.getUsuarios().forEach(usuario -> {
                if (usuario.getRol().getNombre().equalsIgnoreCase(RoleService.ADMINISTRADOR_PRINCIPAL)) {
                    cuentaUsuarioAdministrador[0] = cuentaRepository.findByUsuarioUuid(usuario.getUuid());
                }
            });
            return cuentaUsuarioAdministrador[0];
        }
        return null;
    }

    @Transactional
    public TipoEmpresa getNivelEmpresasAdministradas(String username) {
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
        return tipoDeEmpresaRepository.findByNivel(empresa.getTipo().getNivel() + 1);
    }

    private void validateAndCreateFolders() {
        List<File> foldersdeAcceso =
                Arrays.asList(new File[]{
                        new File(configuracionRutasDeAlmacenamiento.getImagenes()),
                });
        foldersdeAcceso.forEach(folder -> {
            log.info("Path a crear :" + folder.getAbsolutePath());
            if (!folder.exists()) {
                if (folder.mkdirs()) {
                    log.info("Folder creado [" + folder.getAbsolutePath() + "]");
                } else {
                    log.info("error creando folder [" + folder.getAbsolutePath() + "]");
                }
            }
        });
    }

    public InputStream downloadLogo(UUID id) {
        Empresa empresa = empresaRepository.findOne(id);
        if (empresa.getLogo() == null) {
            return null;
        }
        try {
            return new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(configuracionRutasDeAlmacenamiento.getImagenes() + "/" + empresa.getLogo())));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional
    public List<Empresa> getEmpresasAdministradas(String username) {
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
        return empresaRepository.findByEmpresaPadreNitOrderByNombreAsc(empresa.getNit());
    }

    @Transactional
    public Cuenta createAdministrador(String username, Cuenta cuentaAdministrador, UUID uuidEmpresa) {

        validateCuenta(cuentaAdministrador, true, true);
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresaPadre = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
        Empresa empresa = empresaRepository.findOne(uuidEmpresa);
        if (cuentaRepository.findByUsername(cuentaAdministrador.getUsername()) != null) {
            throw new RuntimeException("La cuenta de usuario del administrador no esta disponible. cambie el valor pra la cuenta del usuario.");
        }
        if (empresa == null) {
            throw new RuntimeException("No existe la empresa a la que quiere adicionar el usuario administrador");
        }
        if (empresaPadre == null) {
            throw new RuntimeException("El usuario de la cuenta no tiene asociada la empresa como administrador");
        }
        if (empresa != null && empresaPadre != null) {
            if (empresa.getEmpresaPadre().getUuid() == empresaPadre.getUuid()) {
                cuentaAdministrador.getUsuario().setRol(roleRolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? new Rol(RoleService.ADMINISTRADOR_PRINCIPAL) : roleRolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL));
                cuentaAdministrador.setPassword(bcryptEncoder.encode(cuentaAdministrador.getPassword()));
                cuentaAdministrador = cuentaRepository.save(cuentaAdministrador);
                empresa.getUsuarios().add(cuentaAdministrador.getUsuario());
                empresaRepository.save(empresa);
                return cuentaAdministrador;
            } else {
                throw new RuntimeException("La empresa a la que desea crear el administrado no esta asociada con el administrador de la empresa que la administra");
            }
        }
        return null;
    }

    @Transactional
    public Cuenta updateAdministrador(String username, Cuenta cuentaAdministrador) {

        Cuenta cuentaTmp = cuentaRepository.findOne(cuentaAdministrador.getUuid());
        if (!cuentaTmp.getUsername().equals(cuentaAdministrador.getUsername())) {
            validateCuenta(cuentaAdministrador, true, false);
            cuentaTmp.setUsername(cuentaAdministrador.getUsername());
        }
        if (!cuentaTmp.getPassword().equals(cuentaAdministrador.getPassword())) {
            cuentaTmp.setPassword(bcryptEncoder.encode(cuentaAdministrador.getPassword()));
        }

        if (!cuentaTmp.getUsuario().getNombre().equals(cuentaAdministrador.getUsuario().getNombre())) {
            cuentaTmp.getUsuario().setNombre(cuentaAdministrador.getUsuario().getNombre());
        }

        if (!cuentaTmp.getUsuario().getCedula().equals(cuentaAdministrador.getUsuario().getCedula())) {
            validateUsuario(cuentaAdministrador.getUsuario(), true);
            cuentaTmp.getUsuario().setCedula(cuentaAdministrador.getUsuario().getCedula());
        }

        if (!cuentaAdministrador.getPassword().equals(cuentaTmp.getPassword())) {
            cuentaTmp.setPassword(bcryptEncoder.encode(cuentaAdministrador.getPassword()));
        }

        cuentaTmp.getUsuario().setRol(roleRolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL) == null ? new Rol(RoleService.ADMINISTRADOR_PRINCIPAL) : roleRolRepository.findByNombre(RoleService.ADMINISTRADOR_PRINCIPAL));
        return cuentaRepository.save(cuentaTmp);
    }

    @Transactional
    public void deleteCuentaUsuario(String name, UUID id) {
        Cuenta cuenta = cuentaRepository.findByUsuarioUuid(id);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());

        Boolean indexFound = null;
        for (int i = 0; i < empresa.getUsuarios().size(); i++) {
            if (empresa.getUsuarios().get(i).getUuid().equals(cuenta.getUsuario().getUuid())) {
                empresa.getUsuarios().remove(empresa.getUsuarios().get(i));
                indexFound = true;
                break;
            }
        }
        if (indexFound != null) {
            log.info("Cantidad de usuarios " + empresa.getUsuarios().size());
            empresa = empresaRepository.saveAndFlush(empresa);
        }

        cuentaRepository.delete(cuenta);
    }

    @Transactional
    public List<Cuenta> getUsuariosAdministrador(String username) {
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
        List<Cuenta> cuentas = new ArrayList<>();
        empresa.getUsuarios().forEach(usuario -> {
            cuentas.add(cuentaRepository.findByUsuarioUuid(usuario.getUuid()));
        });
        return cuentas;
    }

    @Transactional
    public List<Rol> getRoles(String username) {
        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());
        List<Rol> roles = new ArrayList<>();
        empresa.getRoles().forEach(rol -> {
            if (cuenta.getUsuario().getRol().getNombre().equals(RoleService.ADMINISTRADOR) && (!rol.getNombre().equals(RoleService.ADMINISTRADOR_PRINCIPAL) && !rol.getNombre().equals(RoleService.ADMINISTRADOR))) {
                roles.add(rol);
            } else if (cuenta.getUsuario().getRol().getNombre().equals(RoleService.ADMINISTRADOR_PRINCIPAL) && !rol.getNombre().equals(RoleService.ADMINISTRADOR_PRINCIPAL)) {
                roles.add(rol);
            }
        });
        return roles;
    }

    @Transactional
    public Cuenta createUsuario(String username, Cuenta cuenta) {
        validateCuenta(cuenta, true, true);

        Cuenta cuentaLogin = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuentaLogin.getUsuario().getUuid());

        cuenta.setPassword(bcryptEncoder.encode(cuenta.getPassword()));

        Rol rolTmp = null;
        if (cuentaLogin.getUsuario().getRol().getNombre().trim().toUpperCase().equals(RoleService.ADMINISTRADOR)) {
            if (cuenta.getUsuario().getRol().getNombre().trim().toUpperCase().equalsIgnoreCase(RoleService.ADMINISTRADOR) ||
                    cuenta.getUsuario().getRol().getNombre().trim().toUpperCase().equalsIgnoreCase(RoleService.ADMINISTRADOR_PRINCIPAL)) {
                throw new RuntimeException("No puede crear cuentas de administrador o administrador principal");
            } else {
                rolTmp = roleRolRepository.findByNombre(
                        cuenta.getUsuario().getRol().getNombre().trim().toUpperCase()) == null ?
                        roleRolRepository.save(new Rol(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase())) :
                        roleRolRepository.findByNombre(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase());
                cuenta.getUsuario().setRol(rolTmp);
            }
        } else if (cuentaLogin.getUsuario().getRol().getNombre().trim().toUpperCase().equals(RoleService.ADMINISTRADOR_PRINCIPAL)) {
            if (cuenta.getUsuario().getRol().getNombre().trim().toUpperCase().equalsIgnoreCase(RoleService.ADMINISTRADOR_PRINCIPAL)) {
                throw new RuntimeException("No puede crear cuentas de administrador principal");
            } else {
                rolTmp = roleRolRepository.findByNombre(
                        cuenta.getUsuario().getRol().getNombre().trim().toUpperCase()) == null ?
                        roleRolRepository.save(new Rol(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase())) :
                        roleRolRepository.findByNombre(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase());
                cuenta.getUsuario().setRol(rolTmp);
            }
        }

        cuenta.getUsuario().setRol(rolTmp);
        cuenta = cuentaRepository.save(cuenta);

        boolean foundRol = false;
        for (Rol rol : empresa.getRoles()) {
            if (rol.getNombre().equalsIgnoreCase(cuenta.getUsuario().getRol().getNombre())) {
                foundRol = true;
                break;
            }
        }
        if (!foundRol) {
            empresa.getRoles().add(rolTmp);
        }
        empresa.getUsuarios().add(cuenta.getUsuario());
        empresaRepository.save(empresa);
        return cuenta;
    }

    @Transactional
    public Cuenta updateUsuario(String username, Cuenta cuenta) {
        validateCuenta(cuenta, false, false);
        Cuenta cuentaTmp = cuentaRepository.findOne(cuenta.getUuid());
        if (!cuentaTmp.getUsername().equals(cuenta.getUsername())) {
            validateCuenta(cuenta, true, false);
            cuentaTmp.setUsername(cuenta.getUsername());
        }
        if (!cuentaTmp.getPassword().equals(cuenta.getPassword())) {
            cuentaTmp.setPassword(bcryptEncoder.encode(cuenta.getPassword()));
        }

        if (!cuentaTmp.getUsuario().getNombre().equals(cuenta.getUsuario().getNombre())) {
            cuentaTmp.getUsuario().setNombre(cuenta.getUsuario().getNombre());
        }

        if (!cuentaTmp.getUsuario().getCedula().equals(cuenta.getUsuario().getCedula())) {
            validateUsuario(cuenta.getUsuario(), true);
            cuentaTmp.getUsuario().setCedula(cuenta.getUsuario().getCedula());
        }
        Cuenta cuentaLogin = cuentaRepository.findByUsername(username);

        Rol rolTmp = null;
        if (cuentaLogin.getUsuario().getRol().getNombre().equals(RoleService.ADMINISTRADOR)) {
            if (!cuentaTmp.getUsuario().getRol().getNombre().trim().toUpperCase().equals(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase())) {
                if (cuenta.getUsuario().getRol().getNombre().trim().toUpperCase().equalsIgnoreCase(RoleService.ADMINISTRADOR) || cuenta.getUsuario().getRol().getNombre().trim().toUpperCase().equalsIgnoreCase(RoleService.ADMINISTRADOR_PRINCIPAL)) {
                    throw new RuntimeException("No puede crear cuentas de administrador o administrador principal");
                } else {
                    rolTmp = roleRolRepository.findByNombre(
                            cuenta.getUsuario().getRol().getNombre().trim().toUpperCase()) == null ?
                            roleRolRepository.save(new Rol(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase())) :
                            roleRolRepository.findByNombre(cuenta.getUsuario().getRol().getNombre().trim().toUpperCase());
                    cuentaTmp.getUsuario().setRol(rolTmp);
                }
            }
        } else if (cuentaLogin.getUsuario().getRol().getNombre().equals(RoleService.ADMINISTRADOR_PRINCIPAL)) {
            if (!cuentaTmp.getUsuario().getRol().getNombre().equals(cuenta.getUsuario().getRol().getNombre())) {
                if (cuenta.getUsuario().getRol().getNombre().equalsIgnoreCase(RoleService.ADMINISTRADOR_PRINCIPAL)) {
                    throw new RuntimeException("No puede crear cuentas de administrador principal");
                } else {
                    rolTmp = roleRolRepository.findByNombre(
                            cuenta.getUsuario().getRol().getNombre()) == null ?
                            roleRolRepository.save(new Rol(cuenta.getUsuario().getRol().getNombre())) :
                            roleRolRepository.findByNombre(cuenta.getUsuario().getRol().getNombre());
                    cuentaTmp.getUsuario().setRol(rolTmp);
                }
            }
        }
        cuentaTmp = cuentaRepository.save(cuentaTmp);
        if (rolTmp != null) {
            Empresa empresa = empresaRepository.findByUsuariosUuid(cuentaLogin.getUsuario().getUuid());
            boolean foundRol = false;
            for (Rol rolEmpresa : empresa.getRoles()) {
                if (rolEmpresa.getNombre().equals(rolTmp.getNombre())) {
                    foundRol = true;
                    break;
                }
            }
            if (!foundRol) {
                empresa.getRoles().add(rolTmp);
            }
            empresaRepository.save(empresa);
        }
        return cuentaTmp;
    }

    private void validateCuenta(Cuenta cuenta, boolean uniqueCuenta, boolean uniqueUsuario) {
        if (cuenta.getUsername() == null || cuenta.getUsername().isEmpty()) {
            throw new RuntimeException("El campo mombre de la cuenta es un valor obligatorio");
        }
        if (cuenta.getPassword() == null || cuenta.getPassword().isEmpty()) {
            throw new RuntimeException("El campo contraseña es un valor obligatorio");
        }
        if (uniqueCuenta) {
            validateCuentUnique(cuenta);
        }
        validateUsuario(cuenta.getUsuario(), uniqueUsuario);
    }

    private void validateCuentUnique(Cuenta cuenta) {
        if (cuentaRepository.findByUsername(cuenta.getUsername()) != null) {
            throw new RuntimeException("El campo mombre de la cuenta " + cuenta.getUsername() + " ya esta registrado. ingrese un valor diferente");
        }
    }

    private void validateUsuario(Usuario usuario, boolean uniqueUsuario) {
        if (usuario.getNombre() == null || usuario.getNombre().isEmpty()) {
            throw new RuntimeException("El campo mombre de usuario es un valor obligatorio");
        }
        if (usuario.getCedula() == null || usuario.getCedula().isEmpty()) {
            throw new RuntimeException("El campo cedula es un valor obligatorio");
        }
        if (uniqueUsuario) {
            validateUsuarioUnique(usuario);
        }
    }

    private void validateUsuarioUnique(Usuario usuario) {
        if (cuentaRepository.findByUsuarioCedula(usuario.getCedula()) != null) {
            throw new RuntimeException("El campo mombre de la cuenta " + usuario.getCedula() + " ya esta registrado. ingrese un valor diferente");
        }
    }

    private void validateEmpresa(Empresa empresa, boolean uniqueEmpresaNombre, boolean uniqueEmpresaNombreNit) {
        if (empresa.getNombre() == null || empresa.getNombre().isEmpty()) {
            throw new RuntimeException("El campo mombre de la empresa es un valor obligatorio");
        }
        if (empresa.getNit() == null || empresa.getNit().isEmpty()) {
            throw new RuntimeException("El campo nit es un valor obligatorio");
        }
        if (uniqueEmpresaNombre) {
            validateEmpresaUniqueNombre(empresa);
        }
        if (uniqueEmpresaNombreNit) {
            validateEmpresaUniqueNit(empresa);
        }
    }

    private void validateEmpresaUniqueNombre(Empresa empresa) {
        if (empresaRepository.findByNombre(empresa.getNombre()) != null) {
            throw new RuntimeException("El campo mombre " + empresa.getNombre() + " ya esta registrado. ingrese un valor diferente");
        }
    }

    private void validateEmpresaUniqueNit(Empresa empresa) {
        if (empresaRepository.findByNit(empresa.getNit()) != null) {
            throw new RuntimeException("El Nit " + empresa.getNit() + " ya esta registrado. ingrese un valor diferente");
        }
    }

    private void validateExtensionFileFile (MultipartFile file){

        if (file !=null){
            String extensionFile = file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length-1].trim().toUpperCase();
            if (extensionFile.equalsIgnoreCase("JPG") || extensionFile.equalsIgnoreCase("PNG")){
                return;
            }else {
                throw new RuntimeException("Solo es permitido la carga de archivos en formato PNG o JPG");
            }
        }

    }
}
