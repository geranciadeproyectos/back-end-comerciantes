package com.kilangsoft.empresa.repository;

import com.kilangsoft.empresa.bo.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa,UUID>{
    Empresa findByUsuariosUuid(UUID uuid);
    Empresa findByNombre(String nombre);
    Empresa findByNit(String nit);
    List<Empresa> findByEmpresaPadreNitOrderByNombreAsc(String nit);
}
