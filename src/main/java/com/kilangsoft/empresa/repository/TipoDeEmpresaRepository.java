package com.kilangsoft.empresa.repository;

import com.kilangsoft.empresa.bo.TipoEmpresa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TipoDeEmpresaRepository extends CrudRepository<TipoEmpresa, UUID> {
    TipoEmpresa findByNombre(String nombre);
    TipoEmpresa findByNivel(int nivel);
}
