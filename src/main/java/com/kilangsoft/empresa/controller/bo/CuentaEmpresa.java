package com.kilangsoft.empresa.controller.bo;

import com.kilangsoft.auth.bo.Cuenta;

import java.util.UUID;


public class CuentaEmpresa {
    private UUID uuid;
    private Cuenta cuenta;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
}
