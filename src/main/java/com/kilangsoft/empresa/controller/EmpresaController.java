package com.kilangsoft.empresa.controller;

import com.google.common.collect.Lists;
import com.kilangsoft.auth.bo.Cuenta;
import com.kilangsoft.empresa.bo.Empresa;
import com.kilangsoft.empresa.bo.TipoEmpresa;
import com.kilangsoft.empresa.controller.bo.CuentaEmpresa;
import com.kilangsoft.empresa.service.EmpresaService;
import com.kilangsoft.auth.bo.Rol;
import com.kilangsoft.usuario.bo.Usuario;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = {"comerciantes/api/v1/empresa"}, produces = {"application/json"})
@Api(value = "/empresa", description = "Operaciones Relacionadas con las empresas", consumes = "application/json")
public class EmpresaController {
    private static final Logger log = Logger.getLogger(EmpresaController.class);

    private EmpresaService empresaService;

    @Autowired
    public EmpresaController(EmpresaService empresaService) {
        this.empresaService = empresaService;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = {RequestMethod.GET}, produces = {"application/json"})
    public Empresa get(OAuth2Authentication auth) {
        return mapObject(empresaService.getEmpresa(auth.getUserAuthentication().getName()));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public Empresa create(OAuth2Authentication auth, @RequestPart("empresa") Empresa empresa, @RequestPart(value = "file", required = false) MultipartFile file) {
        return empresaService.createEmpresa(auth.getUserAuthentication().getName(), empresa, file);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(method = RequestMethod.PUT)
    public Empresa update(OAuth2Authentication auth, @RequestPart("empresa") Empresa empresa, @RequestPart(value = "file", required = false) MultipartFile file) {
        return empresaService.updateEmpresa(auth.getUserAuthentication().getName(), empresa, file);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(OAuth2Authentication auth, @PathVariable("id") UUID id) {
        empresaService.deleteEmpresa(auth.getUserAuthentication().getName(), id);
    }

    @RequestMapping(value = "/logo/{id}", method = RequestMethod.GET)
    public void getLogo(@PathVariable("id") UUID id, HttpServletResponse response) {
        log.info("Descargando imagen para la empresa con el id : " + id);
        try {
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            //response.setContentType("image/jpeg");
            InputStream stream = empresaService.downloadLogo(id);
            if (stream != null) {
                org.apache.commons.io.IOUtils.copy(stream, response.getOutputStream());
            }
            response.flushBuffer();
        } catch (IOException ex) {
            log.info("Error escribiendo el archivo en el flujo de salida del la imagen", ex);
            throw new RuntimeException("Error al retornar el logo de la empresa");
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/sub-empresa", method = {RequestMethod.GET}, produces = {"application/json"})
    public List<Empresa> getEmpresaAdministrada(OAuth2Authentication auth) {
        List<Empresa> empresas = empresaService.getEmpresasAdministradas(auth.getUserAuthentication().getName());
        empresas.forEach(empresa -> {
            empresa.getEmpresaPadre().setEmpresaPadre(null);
            empresa.getEmpresaPadre().setUsuarios(null);
            empresa.getEmpresaPadre().setRoles(null);
            empresa.getEmpresaPadre().setUsuarios(null);
            final Usuario[] administradorPrincipal = {null};
            empresa.getUsuarios().forEach(usuario -> {
                if (usuario.getRol().getNombre().equals("ADMINISTRADOR PRINCIPAL")) {
                    administradorPrincipal[0] = usuario;
                }
            });
            if (administradorPrincipal[0] != null) {
                empresa.setUsuarios(Lists.newArrayList(new Usuario[]{administradorPrincipal[0]}));
            } else {
                empresa.setUsuarios(null);
            }
        });
        return empresas;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/sub-empresa/tipo", method = {RequestMethod.GET}, produces = {"application/json"})
    public TipoEmpresa getNivelEmpresaAdministrada(OAuth2Authentication auth) {
        log.info("Realizando proceso de registro de usuario");
        return empresaService.getNivelEmpresasAdministradas(auth.getUserAuthentication().getName());
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/sub-empresa/administrador", method = {RequestMethod.GET}, produces = {"application/json"})
    public Cuenta getAdministrador(OAuth2Authentication auth, @RequestParam UUID uuid) {
        log.info("Realizando proceso de registro de usuario");
        return empresaService.getUsuarioAdministrador(auth.getUserAuthentication().getName(), uuid);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/sub-empresa/administrador", method = RequestMethod.POST, produces = {"application/json"})
    public Cuenta createAdministrador(OAuth2Authentication auth, @RequestBody CuentaEmpresa cuentaEmpresa) {
        return empresaService.createAdministrador(auth.getUserAuthentication().getName(), cuentaEmpresa.getCuenta(), cuentaEmpresa.getUuid());
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/sub-empresa/administrador", method = RequestMethod.PUT)
    public Cuenta updateAdministrador(OAuth2Authentication auth, @RequestBody Cuenta cuentaAdministrador) {
        return empresaService.updateAdministrador(auth.getUserAuthentication().getName(), cuentaAdministrador);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/sub-empresa/administrador/{id}", method = RequestMethod.DELETE)
    public void deleteAdministrador(OAuth2Authentication auth, @PathVariable("id") UUID id) {
        empresaService.deleteCuentaUsuario(auth.getUserAuthentication().getName(), id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/usuario", method = {RequestMethod.GET}, produces = {"application/json"})
    public List<Cuenta> getUsuarios(OAuth2Authentication auth) {
        log.info("Realizando proceso de obtencion de usuarios");
        return empresaService.getUsuariosAdministrador(auth.getUserAuthentication().getName());
    }


    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/usuario", method = RequestMethod.POST, produces = {"application/json"})
    public Cuenta createUsuario(OAuth2Authentication auth, @RequestBody Cuenta cuenta) {
        return empresaService.createUsuario(auth.getUserAuthentication().getName(), cuenta);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/usuario", method = RequestMethod.PUT)
    public Cuenta updateUsuario(OAuth2Authentication auth, @RequestBody Cuenta cuenta) {
        return empresaService.updateUsuario(auth.getUserAuthentication().getName(), cuenta);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/usuario/{id}", method = RequestMethod.DELETE)
    public void deleteUsuario(OAuth2Authentication auth, @PathVariable("id") UUID id) {
        empresaService.deleteCuentaUsuario(auth.getUserAuthentication().getName(), id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/rol", method = {RequestMethod.GET}, produces = {"application/json"})
    public List<Rol> getRoles(OAuth2Authentication auth) {
        log.info("Realizando proceso de obtencion de roles");
        return empresaService.getRoles(auth.getUserAuthentication().getName());
    }

    private Empresa mapObject(Empresa empresa) {
        return empresa;
    }

    private List<Empresa> mapObject(List<Empresa> empresas) {
        empresas.forEach(empresa -> {
            mapObject(empresa);
        });
        return empresas;
    }
}
