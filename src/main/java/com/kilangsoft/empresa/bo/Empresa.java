package com.kilangsoft.empresa.bo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kilangsoft.auth.bo.Rol;
import com.kilangsoft.usuario.bo.Usuario;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Empresa {

    @Id
    @Column(name = "id")
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    @org.hibernate.annotations.Type(type="pg-uuid")
    private UUID uuid;

    @NotEmpty
    @Column(unique = true,nullable = false)
    private String nombre;


    @NotEmpty
    @Column(unique = true,nullable = false)
    private String nit;

    private String telefono;

    @ManyToOne
    private TipoEmpresa tipo;

    @ManyToOne
    private Empresa empresaPadre;

    @OneToMany
    private List<Usuario> usuarios;

    @ManyToMany
    private List<Rol> roles;

    private String logo;



    public Empresa(String nombre, String nit) {
        this.nombre = nombre;
        this.nit = nit;
    }

    public Empresa(){}

    public Empresa(String nombre, String nit, TipoEmpresa tipo, List<Usuario> usuarios) {
        this.nombre = nombre;
        this.nit = nit;
        this.tipo = tipo;
        this.usuarios = usuarios;
    }

    public Empresa(String nombre, String nit, TipoEmpresa tipo) {
        this.nombre = nombre;
        this.nit = nit;
        this.tipo = tipo;
    }

    public Empresa(String nombre, String nit, TipoEmpresa tipo, List<Usuario> usuarios, List<Rol> roles) {
        this.nombre = nombre;
        this.nit = nit;
        this.tipo = tipo;
        this.usuarios = usuarios;
        this.roles = roles;
    }

    public Empresa(String nombre, String nit, TipoEmpresa tipo, Empresa empresaPadre, List<Usuario> usuarios, List<Rol> roles) {
        this.nombre = nombre;
        this.nit = nit;
        this.tipo = tipo;
        this.empresaPadre = empresaPadre;
        this.usuarios = usuarios;
        this.roles = roles;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public TipoEmpresa getTipo() {
        return tipo;
    }

    public void setTipo(TipoEmpresa tipo) {
        this.tipo = tipo;
    }

    public Empresa getEmpresaPadre() {
        return empresaPadre;
    }

    public void setEmpresaPadre(Empresa empresaPadre) {
        this.empresaPadre = empresaPadre;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return "Empresa{" +
                "uuid=" + uuid +
                ", nombre='" + nombre + '\'' +
                ", nit='" + nit + '\'' +
                ", telefono='" + telefono + '\'' +
                ", tipo=" + tipo +
                ", empresaPadre=" + empresaPadre +
                ", usuarios=" + usuarios +
                ", roles=" + roles +
                '}';
    }
}
