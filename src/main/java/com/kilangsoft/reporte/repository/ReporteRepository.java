package com.kilangsoft.reporte.repository;

import com.kilangsoft.reporte.bo.Reporte;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReporteRepository  extends CrudRepository<Reporte, UUID>{
}
