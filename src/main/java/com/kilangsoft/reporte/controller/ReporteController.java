package com.kilangsoft.reporte.controller;

import com.kilangsoft.reporte.service.ReporteService;
import com.kilangsoft.reporte.bo.Reporte;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = {"comerciantes/api/v1/reporte"}, produces = {"application/json"})
@Api(value = "/reporte", description = "Operaciones Relacionadas con los reportes del sistema", consumes = "application/json")
public class ReporteController {
    private static final Logger log = Logger.getLogger(ReporteController.class);

    private ReporteService reporteService;

    @Autowired
    public ReporteController(ReporteService reporteService) {
        this.reporteService = reporteService;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = {RequestMethod.GET}, produces = {"application/json"})
    public List<Reporte> getAll() {
        return reporteService.getAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public Reporte create(OAuth2Authentication auth, @RequestBody Reporte reporte) {
        return reporteService.create(auth.getUserAuthentication().getName(),reporte);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(OAuth2Authentication auth, @PathVariable("id") UUID id) {
        reporteService.delete(id);
    }
}
