package com.kilangsoft.reporte.service;

import com.google.common.collect.Lists;
import com.kilangsoft.auth.repository.CuentaRepository;
import com.kilangsoft.auth.bo.Cuenta;
import com.kilangsoft.empresa.bo.Empresa;
import com.kilangsoft.empresa.repository.EmpresaRepository;
import com.kilangsoft.reporte.repository.ReporteRepository;
import com.kilangsoft.reporte.bo.Reporte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ReporteService {
    CuentaRepository cuentaRepository;
    EmpresaRepository empresaRepository;
    ReporteRepository reporteRepository;

    @Autowired
    public ReporteService(CuentaRepository cuentaRepository, EmpresaRepository empresaRepository, ReporteRepository reporteRepository) {
        this.cuentaRepository = cuentaRepository;
        this.empresaRepository = empresaRepository;
        this.reporteRepository = reporteRepository;
    }

    public Reporte create(String username, Reporte reporte){
        if (reporte.getTipo()==null || (reporte.getTipo()!=null && reporte.getTipo().isEmpty())){
            throw new RuntimeException("El campo tipo del reporte es obligatorio");
        }
        if (reporte.getObservaciones()==null && reporte.getObservaciones().isEmpty()){
            throw new RuntimeException("El campo observaciones es obligatorio");
        }

        Cuenta cuenta = cuentaRepository.findByUsername(username);
        Empresa empresa = empresaRepository.findByUsuariosUuid(cuenta.getUsuario().getUuid());

        reporte.setCuentaUsuario(cuenta.getUsername());
        reporte.setNombreUsuario(cuenta.getUsuario().getNombre());
        reporte.setCedulaUsuario(cuenta.getUsuario().getCedula());
        reporte.setRolUsuario(cuenta.getUsuario().getRol().getNombre());
        reporte.setNombreEmpresa(empresa.getNombre());
        reporte.setNitEmpresa(empresa.getNit());
        reporte.setTelefonoEmpresa(empresa.getTelefono());
        reporte.setTipoEmpresa(empresa.getTipo().getNombre());
        return reporteRepository.save(reporte);
    }

    public List<Reporte> getAll(){
        return Lists.newArrayList(reporteRepository.findAll());
    }

    public void delete(UUID id){
        reporteRepository.delete(id);
    }
}
